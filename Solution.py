# Enter your code here. Read input from STDIN. Print output to STDOUT
from collections import namedtuple
n = int(input())
fields = input().split()
Student = namedtuple('Student',fields)

sum = 0
for i in range(n):
  field1, field2, field3, field4 = input().split()
  student = Student(field1, field2, field3, field4)
  sum = sum + int(student.MARKS)

print("{:.2f}".format(sum/n))
